STATUS=$(ARG="1 2 3"; ./push_swap $ARG | ./checker $ARG); test "OK" = "$STATUS" || printf "KO three_size 1\n"
STATUS=$(ARG="1 3 2"; ./push_swap $ARG | ./checker $ARG); test "OK" = "$STATUS" || printf "KO three_size 2\n"
STATUS=$(ARG="2 1 3"; ./push_swap $ARG | ./checker $ARG); test "OK" = "$STATUS" || printf "KO three_size 3\n"
STATUS=$(ARG="2 3 1"; ./push_swap $ARG | ./checker $ARG); test "OK" = "$STATUS" || printf "KO three_size 4\n"
STATUS=$(ARG="3 1 2"; ./push_swap $ARG | ./checker $ARG); test "OK" = "$STATUS" || printf "KO three_size 5\n"
STATUS=$(ARG="3 2 1"; ./push_swap $ARG | ./checker $ARG); test "OK" = "$STATUS" || printf "KO three_size 6\n"
