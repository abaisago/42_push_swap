PUSH_SWAP=./push_swap
CHECKER=./checker

if [[ ! -f $PUSH_SWAP || ! -f $CHECKER ]]; then
	printf "$PUSH_SWAP or $CHECKER aren't exist"
	exit 1
fi

while true; do
	RAND=$(ruby -e "puts rand(1..1000)")
	ARG=$(ruby -e "puts (1..$RAND).to_a.shuffle.join(' ')");
	SORT=$(echo $ARG | sed 's/ /\n/g' | sort)
	NO_UNIQ=$(echo "$SORT" | wc -l)
	UNIQ=$(echo "$SORT" | uniq | wc -l)
	$PUSH_SWAP $ARG | $CHECKER $ARG 1>/dev/null
	if [[ $? -eq 1 ]]; then
		if [[ $UNIQ != $NO_UNIQ ]]; then
			echo "$UNIQ - $NO_UNIQ | It's not uniq !"
		fi
		echo $ARG
		break
	fi
done
