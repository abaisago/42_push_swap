/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@protonmail.com>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/21 13:30:25 by abaisago          #+#    #+#             */
/*   Updated: 2020/05/11 13:45:29 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"
#include "libft.h"

#include "common.h"

#include <stdlib.h>

enum e_op	read_op(char *op)
{
	if (!ft_strcmp(op, "sa"))
		return (e_op_sa);
	else if (!ft_strcmp(op, "sb"))
		return (e_op_sb);
	else if (!ft_strcmp(op, "ss"))
		return (e_op_ss);
	else if (!ft_strcmp(op, "pa"))
		return (e_op_pa);
	else if (!ft_strcmp(op, "pb"))
		return (e_op_pb);
	else if (!ft_strcmp(op, "ra"))
		return (e_op_ra);
	else if (!ft_strcmp(op, "rb"))
		return (e_op_rb);
	else if (!ft_strcmp(op, "rr"))
		return (e_op_rr);
	else if (!ft_strcmp(op, "rra"))
		return (e_op_rra);
	else if (!ft_strcmp(op, "rrb"))
		return (e_op_rrb);
	else if (!ft_strcmp(op, "rrr"))
		return (e_op_rrr);
	else
		return ((op[0] == '\0') ? e_op_end : e_op_no);
}

void		execute(t_list *stack_a, t_list *stack_b, enum e_op op)
{
	if (op == e_op_sa)
		sx(stack_a);
	else if (op == e_op_sb)
		sx(stack_b);
	else if (op == e_op_ss)
		ss(stack_a, stack_b);
	else if (op == e_op_pa)
		px(stack_a, stack_b);
	else if (op == e_op_pb)
		px(stack_b, stack_a);
	else if (op == e_op_ra)
		rx(stack_a);
	else if (op == e_op_rb)
		rx(stack_b);
	else if (op == e_op_rr)
		rr(stack_a, stack_b);
	else if (op == e_op_rra)
		rrx(stack_a);
	else if (op == e_op_rrb)
		rrx(stack_b);
	else if (op == e_op_rrr)
		rrr(stack_a, stack_b);
}

void		execute_ops(t_list *stack_a, t_list *stack_b)
{
	char		*input;
	int			ret;
	enum e_op	op;

	while ((ret = get_next_line(0, &input)) > 0)
	{
		op = read_op(input);
		free(input);
		if (op == e_op_no)
			ft_printerr_fd(1, ERROR);
		if (op != e_op_end)
			execute(stack_a, stack_b, op);
	}
	if (ret == -1)
		ft_printerr_fd(1, ERROR);
}

int			checker(int ac, char **av)
{
	t_list	*stack_a;
	t_list	*stack_b;

	if (ac < 2)
		return (0);
	stack_a = args_to_list(ac, av);
	stack_b = ft_list_init();
	if (DEBUG)
		dbg_print_stack(stack_a, stkint_print, "a: ");
	execute_ops(stack_a, stack_b);
	if (DEBUG)
		dbg_print_stack(stack_a, stkint_print, "a: ");
	if (!verify_sort(stack_a, stack_b))
		ft_printerr_fd(1, KO);
	else
		ft_printf(OK);
	ft_list_del(&stack_a, del_stack);
	ft_list_del(&stack_b, del_stack);
	return (0);
}

int			main(int ac, char **av)
{
	checker(ac, av);
	return (0);
}
