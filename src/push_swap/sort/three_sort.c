/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   three_sort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@protonmail.com>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/09 16:58:23 by abaisago          #+#    #+#             */
/*   Updated: 2020/05/11 00:34:37 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include "libft.h"

#include "common.h"

void	three_sort(t_list *s, char sc)
{
	int		first;
	int		second;
	int		third;

	first = ((t_stkint*)s->head->content)->num;
	second = ((t_stkint*)s->head->next->content)->num;
	third = ((t_stkint*)s->head->prev->content)->num;
	if (first > second && second > third)
	{
		do_sx(s, sc);
		do_rrx(s, sc);
	}
	else if (first < second && second > third && third > first)
	{
		do_sx(s, sc);
		do_rx(s, sc);
	}
	else if (first < second && second > third && third < first)
		do_rrx(s, sc);
	else if (first > second && second < third && third > first)
		do_sx(s, sc);
	else if (first > second && second < third && third < first)
		do_rx(s, sc);
}
