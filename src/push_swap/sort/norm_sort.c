/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   norm_sort.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@protonmail.com>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/10 15:01:21 by abaisago          #+#    #+#             */
/*   Updated: 2020/05/11 00:28:13 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include "libft.h"

#include "common.h"

static unsigned	sediment_len(unsigned stack_len)
{
	if (stack_len <= 50)
		return (stack_len / 3);
	else if (stack_len <= 100)
		return (stack_len / 6);
	else if (stack_len <= 300)
		return (stack_len / 10);
	else if (stack_len <= 500)
		return (stack_len / 13);
	else if (stack_len <= 700)
		return (stack_len / 15);
	else if (stack_len <= 900)
		return (stack_len / 17);
	else if (stack_len <= 1000)
		return (stack_len / 19);
	else
		return (stack_len / 25);
}

static void		sediment(t_list *a, t_list *b)
{
	unsigned	range;
	unsigned	range_inc;

	range = 0;
	range_inc = sediment_len(a->len);
	while (a->len >= range_inc)
	{
		range += range_inc;
		while (b->len < range)
		{
			if (((t_stkint*)a->head->content)->norm <= range)
				do_px(b, a, 'b');
			else
				do_rx(a, 'a');
		}
	}
	while (a->len > 0)
		do_px(b, a, 'b');
}

static void		put_back(t_list *to, t_list *from, char sc_to, char sc_from)
{
	unsigned	pos;

	while (from->len > 0)
	{
		if ((pos = ft_list_find_pos(from, &from->len, stknorm_cmp)) == 0)
			ft_printerr_fd(1, "MEAT %u\n", pos);
		do_nrx(pos - 1, from, sc_from);
		do_px(to, from, sc_to);
	}
}

void			norm_sort(t_list *a, t_list *b)
{
	sediment(a, b);
	put_back(a, b, 'a', 'b');
}
