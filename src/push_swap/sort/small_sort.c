/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   small_sort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@protonmail.com>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/12 16:03:51 by abaisago          #+#    #+#             */
/*   Updated: 2020/05/11 11:36:54 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include "libft.h"

#include "common.h"

void	small_sort(t_list *a, t_list *b)
{
	unsigned norm;
	unsigned pos;

	norm = 1;
	while (a->len > 3)
	{
		if ((pos = ft_list_find_pos(a, &norm, stknorm_cmp)) == 0)
			ft_printerr("push_swap: %u not found in five_sort\n", norm);
		do_nrx(pos - 1, a, 'a');
		do_px(b, a, 'b');
		norm += 1;
	}
	three_sort(a, 'a');
	while (b->len > 0)
		do_px(a, b, 'a');
}
