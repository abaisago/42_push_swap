/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   two_sort.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@protonmail.com>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/10 14:55:13 by abaisago          #+#    #+#             */
/*   Updated: 2020/05/10 22:30:10 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include "libft.h"

#include "common.h"

void	two_sort(t_list *s, char sc, unsigned flag)
{
	int		first;
	int		second;

	if (s->len < 2)
		return ;
	first = ((t_stkint*)s->head->content)->num;
	second = ((t_stkint*)s->head->next->content)->num;
	if ((flag == REVERSE) ? first < second : first > second)
		do_sx(s, sc);
}
