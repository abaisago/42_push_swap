/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@protonmail.com>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/05 18:23:19 by abaisago          #+#    #+#             */
/*   Updated: 2020/05/11 12:04:01 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include "libft.h"

#include "common.h"

#include <errno.h>
#include <stdlib.h>
#include <string.h>

static	t_list	*get_normed_list(t_list *stk)
{
	t_list		*normed;
	t_list_link	*link;
	size_t		i;
	size_t		size;

	if ((normed = ft_list_init()) == NULL)
		ft_printerr("push_swap: list_init(malloc fail): %s\n", strerror(errno));
	size = stk->len;
	link = stk->head;
	i = -1;
	while (++i < size)
	{
		ft_list_sorted_insert(normed,
			ft_list_link_new(link->content, link->content_size), stkint_cmp);
		link = link->next;
	}
	link = normed->head;
	i = 0;
	while (++i <= size)
	{
		((t_stkint*)link->content)->norm = i;
		link = link->next;
	}
	return (normed);
}

void			apply_norm(t_list *stk, t_list *norm)
{
	t_list_link *head;
	t_list_link *link;
	t_stkint	*elem;

	head = stk->head;
	link = head;
	while (link->next != head)
	{
		elem = link->content;
		elem->norm =
			((t_stkint*)ft_list_find(norm, elem, stkint_cmp)->content)->norm;
		link = link->next;
	}
	elem = link->content;
	elem->norm =
		((t_stkint*)ft_list_find(norm, elem, stkint_cmp)->content)->norm;
}

void			normalize(t_list *stk)
{
	t_list	*norm;

	norm = get_normed_list(stk);
	apply_norm(stk, norm);
	ft_list_del(&norm, del_stack);
}
