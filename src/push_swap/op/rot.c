/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rot.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@protonmail.com>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/01 18:38:25 by abaisago          #+#    #+#             */
/*   Updated: 2020/03/12 16:22:41 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"
#include "libft.h"

void	do_rx(t_list *s, char sc)
{
	rx(s);
	ft_printf("r%c\n", sc);
}

void	do_rr(t_list *a, t_list *b)
{
	rx(a);
	rx(b);
	ft_putstr("rr\n");
}

void	do_rrx(t_list *s, char sc)
{
	rrx(s);
	ft_printf("rr%c\n", sc);
}

void	do_rrr(t_list *a, t_list *b)
{
	rrx(a);
	rrx(b);
	ft_putstr("rrr\n");
}
