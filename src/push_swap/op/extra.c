/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   extra.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@protonmail.com>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/12 16:19:31 by abaisago          #+#    #+#             */
/*   Updated: 2020/05/11 00:22:52 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include "libft.h"

#include "common.h"

void	do_nrx(unsigned n, t_list *s, char sc)
{
	unsigned	i;

	if (n > s->len)
		return ;
	if (n <= s->len / 2 + 1)
	{
		i = 0;
		while (i++ < n)
			do_rx(s, sc);
	}
	else
	{
		i = s->len;
		while (--i >= n)
			do_rrx(s, sc);
	}
}
