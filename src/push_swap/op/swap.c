/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swap.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@protonmail.com>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/01 17:16:59 by abaisago          #+#    #+#             */
/*   Updated: 2020/03/09 19:34:29 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"
#include "libft.h"

void	do_sx(t_list *s, char sc)
{
	sx(s);
	ft_printf("s%c\n", sc);
}

void	do_ss(t_list *a, t_list *b)
{
	sx(a);
	sx(b);
	ft_putstr("ss\n");
}
