/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@protonmail.com>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/21 13:29:10 by abaisago          #+#    #+#             */
/*   Updated: 2020/05/11 12:13:28 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include "libft.h"

#include "common.h"

#include <errno.h>
#include <string.h>

void	sort_logic(t_list *a, t_list *b)
{
	if (a->len <= 1)
		return ;
	else if (a->len == 2)
		two_sort(a, 'a', NORMAL);
	else if (a->len == 3)
		three_sort(a, 'a');
	else if (a->len <= 10)
		small_sort(a, b);
	else
		norm_sort(a, b);
}

int		push_swap(int ac, char **av)
{
	t_list	*stack_a;
	t_list	*stack_b;

	if (ac < 2)
		return (0);
	stack_a = args_to_list(ac, av);
	if ((stack_b = ft_list_init()) == NULL)
		ft_printerr("push_swap: push_swap(list b malloc): %s\n",
			strerror(errno));
	if (!verify_sort(stack_a, stack_b))
	{
		normalize(stack_a);
		sort_logic(stack_a, stack_b);
	}
	ft_list_del(&stack_a, del_stack);
	ft_list_del(&stack_b, del_stack);
	return (0);
}

int		main(int ac, char **av)
{
	push_swap(ac, av);
	return (0);
}
