/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checks.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@protonmail.com>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/01 09:49:12 by abaisago          #+#    #+#             */
/*   Updated: 2020/05/11 15:47:14 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"
#include "libft.h"

#include <errno.h>
#include <string.h>

static int	int_error(char *str, size_t i, long long num)
{
	size_t	save;

	if (num < -2147483648 || num > 2147483647)
		ft_printerr_fd(1, ERROR);
	while (str[i] == '0')
		++i;
	save = i;
	while (ft_isdigit(str[i]))
		++i;
	if (i - save > 10)
		ft_printerr_fd(1, ERROR);
	return (0);
}

static void	push_to_stack(t_list *stack, int num)
{
	t_list_link	*new;
	t_stkint	elem;

	elem.num = num;
	if ((new = ft_list_link_new(&elem, sizeof(elem))) == NULL)
		ft_printerr("push_swap: push_to_stack(link malloc): %s\n",
			strerror(errno));
	if (ft_list_find(stack, &num, stkint_cmp) != NULL)
		ft_printerr_fd(1, ERROR);
	ft_list_push(stack, new);
}

static void	read_arg(t_list *stack, char *str)
{
	long long	num;
	size_t		i;

	i = 0;
	while (str[i] != '\0')
	{
		while (str[i] == ' ')
			++i;
		if (str[i] == '\0')
			break ;
		if (!ft_isdigit(str[i])
			&& str[i] != '-' && str[i] != '+')
			ft_printerr_fd(1, ERROR);
		num = ft_atoll(str + i);
		push_to_stack(stack, num);
		if (str[i] == '-' || str[i] == '+')
			++i;
		int_error(str, i, num);
		while (ft_isdigit(str[i]))
			++i;
		if (str[i] != ' ' && str[i] != '\0')
			ft_printerr_fd(1, ERROR);
	}
}

t_list		*args_to_list(int ac, char **av)
{
	t_list	*stack;
	int		i;

	if ((stack = ft_list_init()) == NULL)
		ft_printerr("push_swap: args_to_list(list malloc): %s\n",
			strerror(errno));
	i = 1;
	while (i < ac)
	{
		read_arg(stack, av[i]);
		++i;
	}
	return (stack);
}

int			verify_sort(t_list *a, t_list *b)
{
	if (b->len != 0)
		return (0);
	return (ft_list_sorted(a, stkint_cmp));
}
