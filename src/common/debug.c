/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   debug.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@protonmail.com>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/03 21:58:14 by abaisago          #+#    #+#             */
/*   Updated: 2020/05/11 09:21:00 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"
#include "libft.h"

void	dbg_print_stack(t_list *stack,
			int (*print)(void *, size_t, unsigned, unsigned), char *prefix)
{
	if (prefix != NULL)
		ft_printf(prefix);
	ft_printf("%u: ", stack->len);
	ft_list_print(stack, print);
	ft_printf("\n");
}
