/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@protonmail.com>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/01 17:35:11 by abaisago          #+#    #+#             */
/*   Updated: 2020/05/10 22:28:13 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"
#include "libft.h"

void	px(t_list *to, t_list *from)
{
	if (from->len > 0)
		ft_list_push_front(to, ft_list_pop_front(from));
}
