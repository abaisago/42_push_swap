/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swap.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@protonmail.com>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/01 17:16:59 by abaisago          #+#    #+#             */
/*   Updated: 2020/05/10 22:27:44 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"
#include "libft.h"

void	sx(t_list *stack)
{
	t_list_link	*top;
	t_list_link	*second;

	if (stack->len < 2)
		return ;
	if (stack->len == 2)
		stack->head = stack->head->next;
	else
	{
		top = stack->head;
		second = top->next;
		top->prev->next = second;
		second->next->prev = top;
		top->next = second->next;
		second->prev = top->prev;
		second->next = top;
		top->prev = second;
		stack->head = second;
	}
}

void	ss(t_list *stack_a, t_list *stack_b)
{
	sx(stack_a);
	sx(stack_b);
}
