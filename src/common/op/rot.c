/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rot.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@protonmail.com>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/01 18:38:25 by abaisago          #+#    #+#             */
/*   Updated: 2020/03/12 21:22:20 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"
#include "libft.h"

void	rx(t_list *stack)
{
	ft_list_rotate(stack, 1);
}

void	rr(t_list *stack_a, t_list *stack_b)
{
	rx(stack_a);
	rx(stack_b);
}

void	rrx(t_list *stack)
{
	ft_list_rotate(stack, -1);
}

void	rrr(t_list *stack_a, t_list *stack_b)
{
	rrx(stack_a);
	rrx(stack_b);
}
