/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fptr.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@protonmail.com>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/01 15:08:07 by abaisago          #+#    #+#             */
/*   Updated: 2020/05/11 09:25:35 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"

void	del_stack(void *content, size_t content_size)
{
	ft_bzero(content, content_size);
}

int		stkint_cmp(t_stkint *i1, t_stkint *i2)
{
	return (i1->num - i2->num);
}

int		stkint_print(void *content, size_t size, unsigned pos, unsigned total)
{
	t_stkint	*stk;

	stk = (t_stkint*)content;
	(void)size;
	(void)pos;
	(void)total;
	return (ft_printf("[%d]", stk->num));
}

int		stknorm_cmp(t_stkint *i, unsigned *norm)
{
	return (i->norm - *norm);
}

int		stknorm_print(void *content, size_t size, unsigned pos, unsigned total)
{
	t_stkint	*stk;

	stk = (t_stkint*)content;
	(void)size;
	(void)pos;
	(void)total;
	return (ft_printf("[%d]", stk->norm));
}
