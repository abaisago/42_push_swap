/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   common.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@protonmail.com>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/21 23:48:03 by abaisago          #+#    #+#             */
/*   Updated: 2020/05/10 22:10:51 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COMMON_H
# define COMMON_H
# include "libft.h"

# define OK "OK\n"
# define KO "KO\n"
# define ERROR "Error\n"
# define DEBUG 0

typedef struct	s_stkint
{
	int			num;
	unsigned	norm;
}				t_stkint;

enum	e_op
{
	e_op_no,
	e_op_end,
	e_op_sa,
	e_op_sb,
	e_op_ss,
	e_op_pa,
	e_op_pb,
	e_op_ra,
	e_op_rb,
	e_op_rr,
	e_op_rra,
	e_op_rrb,
	e_op_rrr
};

/*
** CHECKS
*/

t_list			*args_to_list(int ac, char **av);
int				verify_sort(t_list *a, t_list *b);

/*
** DEBUG
*/

void			dbg_print_stack(t_list *stack,
					int (*print)(void *, size_t, unsigned, unsigned),
					char *prefix);
void			dbg_print_list(t_list *stack,
					int (*print)(void *, size_t, unsigned), char *prefix);

/*
** FPTR
*/

void			del_stack(void *content, size_t content_size);
int				stknorm_cmp(t_stkint *i, unsigned *norm);
int				stknorm_print(void *content, size_t size,
					unsigned pos, unsigned total);
int				stkint_cmp(t_stkint *i1, t_stkint *i2);
int				stkint_print(void *content, size_t size,
					unsigned pos, unsigned total);

/*
** OPERATIONS
*/

void			sx(t_list *stack);
void			ss(t_list *stack_a, t_list *stack_b);
void			px(t_list *to, t_list *from);
void			rx(t_list *stack);
void			rr(t_list *stack_a, t_list *stack_b);
void			rrx(t_list *stack);
void			rrr(t_list *stack_a, t_list *stack_b);

/*
**************
** COMMON_H	**
**************
*/
#endif
