/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@protonmail.com>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/21 13:32:15 by abaisago          #+#    #+#             */
/*   Updated: 2020/03/17 20:55:26 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H
# include "common.h"

# define NORMAL 0
# define REVERSE 1

void		do_px(t_list *to, t_list *from, char sc);
void		do_sx(t_list *s, char sc);
void		do_ss(t_list *a, t_list *b);
void		do_rx(t_list *s, char sc);
void		do_rr(t_list *a, t_list *b);
void		do_rrx(t_list *s, char sc);
void		do_rrr(t_list *a, t_list *b);
void		do_nrx(unsigned n, t_list *s, char sc);

void		normalize(t_list *stack);

void		two_sort(t_list *s, char sc, unsigned flag);
void		three_sort(t_list *s, char sc);
void		small_sort(t_list *a, t_list *b);

unsigned	norm_find(t_list *a, t_list *b, unsigned range);
void		norm_sort(t_list *a, t_list *b);

int			push_swap(int ac, char **av);

/*
** PUSH_SWAP_H
*/
#endif
