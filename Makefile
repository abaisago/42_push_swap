# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: abaisago <adam_bai@protonmail.com>         +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/12/13 11:54:38 by abaisago          #+#    #+#              #
#    Updated: 2020/05/11 09:15:40 by abaisago         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

######################################################################
#                            DEFINITIONS                             #
######################################################################

#------------------------------------------------#
#                     PROJECT                    |
#------------------------------------------------#

CHECK          := checker
PUSH           := push_swap
NAME           := $(CHECK) $(PUSH)
PROJECT        := push_swap

#------------------------------------------------#
#                   LIBRARIES                    |
#------------------------------------------------#

LIB_PATH       := lib

LIB_FT_DIR     := $(LIB_PATH)/ft
LIB_FT_NAME    := libft.a
LIB_FT         := $(LIB_FT_DIR)/$(LIB_FT_NAME)
LIB_FT_FLAGS   := -lft

LIB            := $(LIB_FT)

LDFLAGS        := -L$(LIB_FT_DIR)
LDLIBS         := $(LIB_FT_FLAGS)

#------------------------------------------------#
#                   BINARIES                     |
#------------------------------------------------#

CC             := gcc
CFLAGS         := -Wall -Wextra -Werror
CPPFLAGS       := -Iinclude                      \
                  -Ilib/ft/include

AR             := /usr/bin/ar
ARFLAGS        := rc
MAKE           := /usr/bin/make
RANLIB         := /usr/bin/ranlib

CP             := /bin/cp
NORMINETTE     := /usr/bin/norminette
MKDIR          := /bin/mkdir
PRINTF         := /usr/bin/printf
RM             := /bin/rm

#------------------------------------------------#
#                    SOURCES                     |
#------------------------------------------------#

CM_OP          := push.c rot.c swap.c
CM_OP          := $(addprefix op/,$(CM_OP))
CM             := checks.c debug.c fptr.c        \
                  $(CM_OP)
CM             := $(addprefix common/,$(CM))

CH             := main.c
CH             := $(addprefix checker/,$(CH))

PS_OP          := extra.c push.c rot.c swap.c
PS_OP          := $(addprefix op/,$(PS_OP))
PS_SORT        := two_sort.c                     \
                  three_sort.c                   \
                  small_sort.c                   \
                  norm_sort.c
PS_SORT        := $(addprefix sort/,$(PS_SORT))
PS             := main.c                         \
                  stack.c                        \
                  $(PS_OP)                       \
                  $(PS_SORT)
PS             := $(addprefix push_swap/,$(PS))

SRC_PATH       := src

SRC_NAME_CM    := $(CM)
SRC_COMMON     := $(addprefix src/,$(SRC_NAME_CM))

SRC_NAME_CH    := $(CH)
SRC_CHECK      := $(addprefix src/,$(SRC_NAME_CH))

SRC_NAME_PS    := $(PS)
SRC_PUSH       := $(addprefix src/,$(SRC_NAME_PS))

OBJ_PATH       := obj
OBJ_NAME_CM    := $(SRC_NAME_CM:.c=.o)
OBJ_NAME_CH    := $(SRC_NAME_CH:.c=.o)
OBJ_NAME_PS    := $(SRC_NAME_PS:.c=.o)
OBJ_COMMON     := $(addprefix $(OBJ_PATH)/,$(OBJ_NAME_CM))
OBJ_CHECK      := $(addprefix $(OBJ_PATH)/,$(OBJ_NAME_CH))
OBJ_PUSH       := $(addprefix $(OBJ_PATH)/,$(OBJ_NAME_PS))

#------------------------------------------------#
#                    RELEASE                     |
#------------------------------------------------#

REL_PATH       := release
CHECK          := $(CHECK)
PUSH           := $(PUSH)
REL_OBJ_COMMON := $(addprefix $(REL_PATH)/,$(OBJ_COMMON))
REL_OBJ_CHECK  := $(addprefix $(REL_PATH)/,$(OBJ_CHECK))
REL_OBJ_PUSH   := $(addprefix $(REL_PATH)/,$(OBJ_PUSH))
REL_CFLAGS     := $(CFLAGS)

#------------------------------------------------#
#                     DEBUG                      |
#------------------------------------------------#

DBG_PATH       := debug
DBG_CHECK      := $(DBG_PATH)/$(CHECK)
DBG_PUSH       := $(DBG_PATH)/$(PUSH)
DBG            := $(DBG_CHECK) $(DBG_PUSH)
DBG_OBJ_COMMON := $(addprefix $(DBG_PATH)/,$(OBJ_COMMON))
DBG_CHECK_OBJ  := $(addprefix $(DBG_PATH)/,$(OBJ_CHECK))
DBG_PUSH_OBJ   := $(addprefix $(DBG_PATH)/,$(OBJ_PUSH))
DBG_CFLAGS     := $(CFLAGS) -g -fsanitize=address

#------------------------------------------------#
#                     EXTRA                      |
#------------------------------------------------#

EOC         := "\033[0;0m"
RED         := "\033[0;31m"
GREEN       := "\033[0;32m"

N           := $(CR)$(GREEN)
D           := $(CR)$(RED)
E           := $(EOC)"\033[0K\n"
UP          := "\033[0F"



######################################################################
#                               RULES                                #
######################################################################
.PHONY: all, clean, fclean, re, release, debug
.SILENT:

all: release

mdebug: 
	printf "d> $(REL_OBJ_PUSH)\n"

#------------------------------------------------#
#                 RELEASE-RULES                  |
#------------------------------------------------#

release: $(CHECK) $(PUSH)

$(CHECK): $(REL_OBJ_COMMON) $(REL_OBJ_CHECK) $(LIB)
	@$(PRINTF) $(N)"[ $(PROJECT): $(CHECK) object files created ]"$(E)
	@$(CC) $(REL_CFLAGS) -o $@ $^ $(LDFLAGS) $(LDLIBS)
	@$(PRINTF) $(N)"[ $(PROJECT): $@ created ]"$(E)

$(PUSH): $(REL_OBJ_COMMON) $(REL_OBJ_PUSH) $(LIB)
	@$(PRINTF) $(N)"[ $(PROJECT): $(PUSH) object files created ]"$(E)
	@$(CC) $(REL_CFLAGS) -o $@ $^ $(LDFLAGS) $(LDLIBS) $(REL_CFLAGS)
	@$(PRINTF) $(N)"[ $(PROJECT): $@ created ]"$(E)

$(REL_PATH)/$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	@$(MKDIR) -p $(dir $@) 2>/dev/null || true
	@$(PRINTF) "[ $(PROJECT): %s ]"$(E) $@
	@$(CC) $(REL_CFLAGS) $(CPPFLAGS) -c $< -o $@
	@$(PRINTF) $(UP)

#------------------------------------------------#
#                  DEBUG-RULES                   |
#------------------------------------------------#

dbg: $(DBG_CHECK) $(DBG_PUSH)

$(DBG_CHECK): $(DBG_OBJ_COMMON) $(DBG_CHECK_OBJ) $(LIB)
	@$(PRINTF) $(N)"[ $(PROJECT): $(CHECK) debug object files created ]"$(E)
	@$(CC) $(DBG_CFLAGS) -o $@ $^ $(LDFLAGS) $(LDLIBS)
	@$(PRINTF) $(N)"[ $(PROJECT): $@ created ]"$(E)

$(DBG_PUSH): $(DBG_OBJ_COMMON) $(DBG_PUSH_OBJ) $(LIB)
	@$(PRINTF) $(N)"[ $(PROJECT): $(PUSH) debug object files created ]"$(E)
	@$(CC) $(DBG_CFLAGS) -o $@ $^ $(LDFLAGS) $(LDLIBS)
	@$(PRINTF) $(N)"[ $(PROJECT): $@ created ]"$(E)

$(DBG_PATH)/$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	@$(MKDIR) -p $(dir $@) 2>/dev/null || true
	@$(PRINTF) "[ $(PROJECT): %s ]"$(E) $@
	@$(CC) $(DBG_CFLAGS) $(CPPFLAGS) -c $< -o $@
	@$(PRINTF) $(UP)

#------------------------------------------------#
#                 LIBRARY-RULES                  |
#------------------------------------------------#

### LIBFT

libft: $(LIB_FT)
$(LIB_FT): FORCE
	make -sC $(LIB_FT_DIR)

libft_clean:
	make -sC $(LIB_FT_DIR) clean

libft_dbgclean:
	make -sC $(LIB_FT_DIR) dbgclean

libft_fclean:
	make -sC $(LIB_FT_DIR) fclean

### Library Clean Rules

libclean: libft_clean
libdbgclean: libft_dbgclean
libfclean: libft_fclean

#------------------------------------------------#
#                  CLEAN-RULES                   |
#------------------------------------------------#

thisclean:
	@if [ -d $(REL_PATH)/$(OBJ_PATH) ]; then \
		$(RM) -f $(REL_OBJ) \
		&& $(RM) -rf $(REL_PATH)/$(OBJ_PATH) \
		&& $(PRINTF) $(D)"[ $(PROJECT): All object files cleaned ]"$(E); \
	fi

thisdbgclean:
	@if [ -d $(DBG_PATH)/$(OBJ_PATH) ]; then \
		$(RM) -f $(DBG_OBJ) \
		&& $(RM) -rf $(DBG_PATH)/$(OBJ_PATH) \
		&& $(PRINTF) $(D)"[ $(PROJECT): All debug object files cleaned ]"$(E); \
	fi

clean: libclean thisclean
dbgclean: libclean thisdbgclean

fclean: libfclean thisclean thisdbgclean
	@for TARGET in $(NAME) $(DBG); do \
		if [ -e "$$TARGET" ]; then \
			$(RM) -f $$TARGET \
			&& $(PRINTF) $(D)"[ $(PROJECT): $$TARGET cleaned ]"$(E); \
		fi; \
	done
ifneq '$(REL_PATH)' '.'
	@$(RM) -rf $(REL_PATH)
endif	
ifneq '$(DBG_PATH)' '.'
	@$(RM) -rf $(DBG_PATH)
endif	

#------------------------------------------------#
#                  OTHER-RULES                   |
#------------------------------------------------#

soft: thisclean all
softdbg: thisdbgclean dbg
shallow: soft softdbg
re: fclean all
redbg: fclean dbg
full: fclean all dbg

neat: all clean
neatdbg: dbg dbgclean
pure: clear cleardbg
whole: all dbg

FORCE:

norme:
	$(NORMINETTE) $(SRC) include/*.h

update_libft:
	$(RM) -rf $(LIB_FT_DIR)
	git clone http://gitlab.com/abaisago/42_libft $(LIB_FT_DIR)
	$(RM) -rf $(LIB_FT_DIR)/.git* $(LIB_FT_DIR)/tags

LIB_HT_DIR := lib/ht
update_libht:
	$(RM) -rf $(LIB_HT_DIR)
	git clone http://gitlab.com/abaisago/libht $(LIB_HT_DIR)
	$(RM) -rf $(LIB_HT_DIR)/.git* $(LIB_HT_DIR)/tags
